module ir.iman3in {
    requires javafx.controls;
    requires javafx.fxml;

    opens ir.iman3in to javafx.fxml;
    exports ir.iman3in;
}